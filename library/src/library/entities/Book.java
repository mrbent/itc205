/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;

    public class Book implements IBook {

    
    /*
       author:String, title:String, callNumber:String, bookID:int 
    throws IllegalArgumentException if:  
    any of author, title, or callNumber are null or blank || 
    bookID is less than or equal to zero 
    */
    private String author;
    private String title;
    private String callNumber;
    private int bookID;
    ILoan loan;
    EBookState state;
    
    public Book(String author, String title, String callNumber, int bookID) {
    
        if(author.equals(null)||author.equals("")||title.equals(null)||title.equals("")||callNumber.equals(null)||callNumber.equals("")||bookID<=0){
            throw new IllegalArgumentException("Error object not initialised");          
    }
             this.author = author;
             this.title=title;
             this.callNumber=callNumber;
             this.bookID=bookID;
             this.loan=loan;
             this.state=EBookState.AVAILABLE;
    
}
    @Override
    public void borrow(ILoan loan) {
        /*
        associates the loan with the book 
        throws RuntimeException if: 
        the book is not currently AVAILABLE 
        */
       
        if(state!=EBookState.AVAILABLE){
            throw new RuntimeException("cannot loan book is not available");
        }
         state =EBookState.ON_LOAN;
        
    }
    @Override
    public ILoan getLoan(){
        /*
        getLoan: ILoan 
        returns the loan associated with the book 
        returns null if:
 	the book is not current ON_LOAN
        */
        if(state==EBookState.ON_LOAN){
        return loan;
        }
        
        return null;
    }
    @Override
    public void returnBook(boolean damaged) {
        /*
         removes the loan the book is associated with if: 
         damaged sets the BookState to DAMAGED 
         else 
         sets the BookState to AVAILABLE 
         throws RuntimeException if: 
         the book is not currently ON_LOAN 
        */
      
        
        
        if(state!=EBookState.ON_LOAN){
                throw new RuntimeException("Book is " + state);
            }
        if(damaged){
            state =EBookState.DAMAGED;
        }
        else{
            state = EBookState.AVAILABLE;
            
            
        }
    }
    
    
   
    
    @Override
    public void lose(){
        /*
         sets the BookState to LOST 
         throws RuntimeException if: 
         the book is not currently ON_LOAN 
 
        */
        if(state!=EBookState.ON_LOAN){
            throw new RuntimeException("Book is not on loan!");
        }
         state=EBookState.LOST;
        
    }
    
    @Override
    public void repair(){
        /*
          sets the BookState to AVAILABLE 
          throws RuntimeException if:  the book is not currently DAMAGED 
        */
       
        
        if(state!=EBookState.DAMAGED){
             throw new RuntimeException("Book is " + state);
        }
         state =EBookState.AVAILABLE;
        
    }
    @Override
    public void dispose(){
        /*
        sets the BookState to DISPOSED 
        throws RuntimeException if: 
        the book is not currently AVAILABLE, DAMAGED, or LOST 

        */
        
        
        if((state!=EBookState.DAMAGED)^(state!=EBookState.AVAILABLE)^(state!=EBookState.LOST)){
             throw new RuntimeException("Book is " + state);
        }
        state=EBookState.DISPOSED;
    }
    
    @Override
    public EBookState getState(){
        //returns the book’s current BookState 

        return state;

        
    }
    
    public String getAuthor(){
        //returns the book’s author 
        return author;
    }
    
    @Override
    public String getTitle(){
        //returns the book’s title 
        return title;
    }
    public String getCallNumber(){
        //returns the call number 
        return callNumber;
    }
    @Override
    public int getID(){
        //returns the bookID
        return bookID;
    }

    

    

    
    
    
    
    
}