package library;

import static com.sun.medialib.mlib.Image.Add;
import library.hardware.CardReader;
import library.hardware.Display;
import library.hardware.Printer;
import library.hardware.Scanner;

import java.util.Calendar;
import java.util.Date;
import library.daos.BookHelper;
import library.daos.BookMapDAO;
import library.daos.LoanHelper;
import library.daos.LoanMapDAO;
import library.daos.MemberHelper;
import library.daos.MemberMapDAO;

import library.interfaces.IMainListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.panels.MainPanel;

public class Main implements IMainListener {

	private CardReader reader;
	private Scanner scanner;
	private Printer printer;
	private Display display;
	private IBookDAO bookDAO;
	private ILoanDAO loanDAO;
	private IMemberDAO memberDAO;
	
	public Main() {
		reader = new CardReader();
		scanner = new Scanner();
		printer = new Printer();
		display = new Display();
		
		//setupTestData();
	}


	public void showGUI() {		
		reader.setVisible(true);
		scanner.setVisible(true);
		printer.setVisible(true);
		display.setVisible(true);
	}

	
	@Override
	public void borrowBooks() {
		BorrowUC_CTL ctl = new BorrowUC_CTL(reader, scanner, printer, display, 
				 bookDAO, loanDAO, memberDAO);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	ctl.initialise();
            }
        });		
	}

	
	private void setupTestData() {
                
            
             BookHelper bookHelper = new BookHelper();
             LoanHelper loanHelper = new LoanHelper();
            MemberHelper memberHelper = new MemberHelper();
           
            bookDAO = new BookMapDAO(bookHelper);
            loanDAO = new LoanMapDAO(loanHelper);
            memberDAO = new MemberMapDAO(memberHelper);
            
            
            
                IBook[] book = new IBook[15];
		IMember[] member = new IMember[6];
		
		book[0]  = bookDAO.addBook("author1", "title1", "callNo1");
		book[1]  = bookDAO.addBook("author1", "title2", "callNo2");
		book[2]  = bookDAO.addBook("author1", "title3", "callNo3");
		book[3]  = bookDAO.addBook("author1", "title4", "callNo4");
		book[4]  = bookDAO.addBook("author2", "title5", "callNo5");
		book[5]  = bookDAO.addBook("author2", "title6", "callNo6");
		book[6]  = bookDAO.addBook("author2", "title7", "callNo7");
		book[7]  = bookDAO.addBook("author2", "title8", "callNo8");
		book[8]  = bookDAO.addBook("author3", "title9", "callNo9");
		book[9]  = bookDAO.addBook("author3", "title10", "callNo10");
		book[10] = bookDAO.addBook("author4", "title11", "callNo11");
		book[11] = bookDAO.addBook("author4", "title12", "callNo12");
		book[12] = bookDAO.addBook("author5", "title13", "callNo13");
		book[13] = bookDAO.addBook("author5", "title14", "callNo14");
		book[14] = bookDAO.addBook("author5", "title15", "callNo15");
		
		member[0] = memberDAO.addMember("fName0", "lName0", "0001", "email0");
		member[1] = memberDAO.addMember("fName1", "lName1", "0002", "email1");
		member[2] = memberDAO.addMember("fName2", "lName2", "0003", "email2");
		member[3] = memberDAO.addMember("fName3", "lName3", "0004", "email3");
		member[4] = memberDAO.addMember("fName4", "lName4", "0005", "email4");
		member[5] = memberDAO.addMember("fName5", "lName5", "0006", "email5");
		
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
				
		
                
	}

	
	public static void main(String[] args) {
		
        // start the GUI
		Main main = new Main();
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	main.display.setDisplay(new MainPanel(main), "Main Menu");
                main.showGUI();
            }
        });
	}

	
}
