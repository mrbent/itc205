package library;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import library.interfaces.EBorrowState;
import library.interfaces.IBorrowUI;
import library.interfaces.IBorrowUIListener;
import library.interfaces.daos.IBookDAO;
import library.interfaces.daos.ILoanDAO;
import library.interfaces.daos.IMemberDAO;
import library.interfaces.entities.EBookState;
import library.interfaces.entities.EMemberState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import library.interfaces.hardware.ICardReader;
import library.interfaces.hardware.ICardReaderListener;
import library.interfaces.hardware.IDisplay;
import library.interfaces.hardware.IPrinter;
import library.interfaces.hardware.IScanner;
import library.interfaces.hardware.IScannerListener;

public class BorrowUC_CTL implements ICardReaderListener, 
									 IScannerListener, 
									 IBorrowUIListener {
	
	private ICardReader reader;
	private IScanner scanner; 
	private IPrinter printer; 
	private IDisplay display;
	//private String state;
	private int scanCount = 0;
	private IBorrowUI ui;
	private EBorrowState state; 
	private IBookDAO bookDAO;
	private IMemberDAO memberDAO;
	private ILoanDAO loanDAO;
	
	private List<IBook> bookList = new ArrayList();
	private List<ILoan> loanList= new ArrayList();
	private IMember borrower;
	
	private JPanel previous;


	public BorrowUC_CTL(ICardReader reader, IScanner scanner, 
			IPrinter printer, IDisplay display,
			IBookDAO bookDAO, ILoanDAO loanDAO, IMemberDAO memberDAO ) {

		this.printer=printer;
                this.display = display;
                this.reader = reader;
                
                this.bookDAO=bookDAO;
                this.loanDAO = loanDAO;
                this.memberDAO=memberDAO;
                
		this.ui = new BorrowUC_UI(this);
		state = EBorrowState.CREATED;
	}
	
	public void initialise() {
		previous = display.getDisplay();
		display.setDisplay((JPanel) ui, "Borrow UI");
                
                borrower=null;
                scanCount=0;
                
                reader.setEnabled(true);
                scanner.setEnabled(false);
                
                reader.addListener(this);
                scanner.addListener(this);
                
                ui.setState(EBorrowState.INITIALIZED);
                state = EBorrowState.INITIALIZED;
                
               
	}
	
	public void close() {
		display.setDisplay(previous, "Main Menu");
	}
//implimentated by ben
	@Override
	public void cardSwiped(int memberID) {
		 
           
           if(state!=EBorrowState.INITIALIZED){
                throw new RuntimeException("The State is Not Initialized");
           }      
                 
            if(memberDAO==null){
                throw new RuntimeException("The Member DAO is Null");
            }
            
            if(memberDAO.getMemberByID(memberID)==null){
                ui.displayErrorMessage("Member not found!");
                
            }
            
            borrower = memberDAO.getMemberByID(memberID);
            
            if(borrower.getState()==EMemberState.BORROWING_DISALLOWED){
                reader.setEnabled(false);
                    scanner.setEnabled(false);
                    
                    ui.displayErrorMessage(borrower.getFirstName() + " Member Cannot Borrow");
                    
                    if(borrower.hasFinesPayable()){
                        ui.displayOutstandingFineMessage(borrower.getFineAmount());
                    }
                    
                    if(borrower.hasOverDueLoans()){
                        ui.displayOverDueMessage();
                    }
                    if(borrower.hasReachedLoanLimit()){
                        ui.displayAtLoanLimitMessage();
                    }
                    
                    
                
                    
            }
            if(borrower.getState()!=EMemberState.BORROWING_DISALLOWED){
                
                    reader.setEnabled(true);
                    scanner.setEnabled(true);
                    previous = display.getDisplay();
                    scanCount = borrower.getLoans().size();
                
                    if(borrower.hasFinesPayable())
                        ui.displayOutstandingFineMessage(borrower.getFineAmount());
                    
                    ui.setState(EBorrowState.SCANNING_BOOKS);
                    state = EBorrowState.SCANNING_BOOKS;
            }
            
	}
	
	
	
	@Override
	public void bookScanned(int barcode) {
            IBook book = bookDAO.getBookByID(barcode);
            
            if(book==null){
                ui.displayErrorMessage("Cannot find " + book.getTitle() );
                setState(EBorrowState.SCANNING_BOOKS);
                return;
            }
            if(book.getState()!=EBookState.AVAILABLE){
                 ui.displayErrorMessage(book.getTitle() + " is not available" );
                 setState(EBorrowState.SCANNING_BOOKS);
                 return;
                
            }
            if(bookList.contains(book)){
                 ui.displayErrorMessage(book.getTitle() + " is already scanned" );
                  return;
                
            }
            if(scanCount < IMember.LOAN_LIMIT){
                
                ILoan loan = loanDAO.createLoan(borrower, book);
                
                scanCount++;
                String bookDetails = book.toString();
                ui.displayScannedBookDetails(bookDetails);
                bookList.add(book);
                loanList.add(loan);
                String loanDeatails =loan.toString();
                ui.displayPendingLoan(loanDeatails);
                setState(EBorrowState.SCANNING_BOOKS);
                
            }
            if(scanCount==IMember.LOAN_LIMIT){
                previous = display.getDisplay();
                scanner.setEnabled(false);
                reader.setEnabled(false);
                setState(EBorrowState.CONFIRMING_LOANS);
            }
            
            
            
            
            
            
	}

	
	private void setState(EBorrowState state) {
		this.state=state;
                ui.setState(state);
	}

	@Override
	public void cancelled() {
		close();
                bookList.clear();
                loanList.clear();
                
	}
	
	@Override
	public void scansCompleted() {
            previous = display.getDisplay();
            for(int i=0; i<loanList.size();i++){
                String loanDetails=loanList.get(i).toString();
                ui.displayPendingLoan(loanDetails);
            }
            scanner.setEnabled(false);
            reader.setEnabled(false);
            setState(EBorrowState.COMPLETED);
            
	}

	@Override
	public void loansConfirmed() {
            
             for(int i=0; i<loanList.size();i++){
                 loanDAO.commitLoan(loanList.get(i));
             }
            scanner.setEnabled(false);
            reader.setEnabled(false);
            setState(EBorrowState.COMPLETED);
            
	}

	@Override
	public void loansRejected() {
		loanList.clear();
                scanCount=borrower.getLoans().size();
                scanner.setEnabled(true);
                reader.setEnabled(false);
                setState(EBorrowState.SCANNING_BOOKS);
	}

	private String buildLoanListDisplay(List<ILoan> loans) {
		StringBuilder bld = new StringBuilder();
		for (ILoan ln : loans) {
			if (bld.length() > 0) bld.append("\n\n");
			bld.append(ln.toString());
		}
		return bld.toString();		
	}

}
