/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import library.interfaces.entities.EMemberState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


/**
 *
 * @author bendev
 */
public class MemberTest {
    
     Member instance;
    
    public MemberTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of hasOverDueLoans method, of class Member.
     */
    @Test
    public  void testHasOverDueLoans() {
        System.out.println("hasOverDueLoans");
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        
        Loan loan = new Loan(book,instance, borrowDate, dueDate);
        Date today= new Date();
        loan.commit(1);
        loan.checkOverDue(today);
       
        
       
        boolean expResult =instance.hasOverDueLoans();
        System.out.println("\n result is" + expResult);
        instance.hasOverDueLoans();
       
       
        Boolean result = instance.hasOverDueLoans();
        assertTrue(expResult);
        
    }
    

    /**
     * Test of hasReachedLoanLimit method, of class Member.
     */
    @Test
    public void testHasReachedLoanLimit() {
        System.out.println("hasReachedLoanLimit");
        
       instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        Loan loan = new Loan(book,instance, borrowDate, dueDate);
        
        for(int i=0; i<5;i++){
            instance.addLoan(loan);
            
        }
        boolean expResult = instance.hasReachedLoanLimit();
        assertTrue(expResult);
        
    }

    /**
     * Test of hasFinesPayable method, of class Member.
     */
    @Test
    public void testHasFinesPayable() {
        System.out.println("hasFinesPayable");
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.addFine(10);
        boolean expResult = instance.hasFinesPayable();
       
        assertTrue(expResult);
        
    }

    /**
     * Test of hasReachedFineLimit method, of class Member.
     */
    @Test
    public void testHasReachedFineLimit() {
        System.out.println("hasReachedFineLimit");
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.addFine(11);
        boolean expResult = instance.hasReachedFineLimit();
       
        assertTrue(expResult);
        
    }

    /**
     * Test of addFine method, of class Member.
     */
    @Test
    public void testAddFine() {
        System.out.println("addFine");
        float fine = 1.0F;
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.addFine(fine);
        float expResult= instance.getFineAmount();
        assertEquals(expResult,fine,0.0F);
    }

    /**
     * Test of payFine method, of class Member.
     */
    @Test
    public void testPayFine() {
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        System.out.println("payFine");
        float payment = 1.0F;
        instance.addFine(payment);
        instance.payFine(payment);
        float expResult= instance.getFineAmount();
        assertEquals(0.0F,expResult,0.0F);
    }

    /**
     * Test of addLoan method, of class Member.
     */
    @Test
    public void testAddLoan() {
        System.out.println("addLoan");
       
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
         
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        
        Loan loan = new Loan(book,instance, borrowDate, dueDate);
         
        instance.addLoan(loan);
        
        assertEquals(loan,instance.getLoans().get(0));
        
    }

    

    /**
     * Test of removeLoan method, of class Member.
     */
    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveLoan() {
        System.out.println("removeLoan");
        
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        
        Loan loan = new Loan(book,instance, borrowDate, dueDate);
        instance.addLoan(loan);
        instance.removeLoan(loan);
        
        assertEquals(loan,instance.getLoans().get(0));
        
    }
    /**
     * Test of toString method, of class Member.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.addFine(1);
        String expResult = "Id: 42\n" +
"Name: Billy Bookington\n" +
"Contact Phone: 555-999-839\n" +
"Email: thisIsMyEmail@email.net\n" +
"Outstanding Charges: 1.00.2f";
               
                
        String result = instance.toString();
        assertEquals(expResult, result);
       
    }
    
}
