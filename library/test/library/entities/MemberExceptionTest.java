/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class MemberExceptionTest {
    
    public MemberExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

   
   
    

    
    
    /**
     * Test of addFine method, of class Member.
     */
    @Test(expected = RuntimeException.class)
    public void testAddFine() {
        System.out.println("addFine");
        float fine = -1.0F;
        Member instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.addFine(fine);
        
    }

    /**
     * Test of payFine method, of class Member.
     */
    @Test(expected = RuntimeException.class)
    public void testPayFine() {
        System.out.println("payFine");
        float payment = -1.0F;
        Member instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        instance.payFine(payment);
        
    }

    /**
     * Test of addLoan method, of class Member.
     */
    @Test(expected = RuntimeException.class)
    public void testAddLoan() {
        System.out.println("addLoan");
        Member instance =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
      
        Loan loan = null;
       
       instance.addLoan(loan);
       assertEquals(loan,instance.getLoans().get(0));
        
    }    
}
