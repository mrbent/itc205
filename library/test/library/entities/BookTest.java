
package library.entities;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.ILoan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class BookTest {
    
    String author ="Writer Jackson";
    String title= "of drawers and piece ";
    String callNumber ="B42";
    int bookID=1;
    
    public BookTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of borrow method, of class Book.
     */
    @Test
    public void testBorrow() {
        System.out.println("borrow");
        ILoan loan = null;
        
        Book instance = new Book(author, title, callNumber, bookID);
        instance.borrow(loan);
        EBookState expResult=EBookState.ON_LOAN;
        assertEquals(expResult,instance.getState());
    }

    /**
     * Test of getLoan method, of class Book.
     */
    @Test
    public void testGetLoan() {
        System.out.println("getLoan");
        Book instance = new Book(author, title, callNumber, bookID);
        ILoan expResult = null;
        instance.borrow(expResult);
        ILoan result = instance.getLoan();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of returnBook method, of class Book.
     */
    @Test
    public void testReturnBookNotDamaged() {
        System.out.println("returnBook");
        boolean damaged =false;
        Book instance = new Book(author, title, callNumber, bookID);
        ILoan loan = null;
        instance.borrow(loan);
        instance.returnBook(damaged);
        EBookState expResult = EBookState.AVAILABLE;
        EBookState result = instance.getState();
 
        assertEquals(expResult, result);
        
    }

    /**
     * Test of ReturnBook method, of class Book.
     */
    @Test
    public void testReturnBookIsDamaged() {
        System.out.println("ReturnBook");
        boolean damaged = true;
        Book instance = new Book(author, title, callNumber, bookID);
        ILoan loan =null;
        instance.borrow(loan);
        instance.returnBook(damaged);
        EBookState expResult = EBookState.DAMAGED;
        EBookState result = instance.getState();
        
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of lose method, of class Book.
     */
    @Test
    public void testLose() {
        System.out.println("lose");
         Book instance = new Book(author, title, callNumber, bookID);
         ILoan loan = null;
         instance.borrow(loan);
         
         instance.lose();
         EBookState expResult = EBookState.LOST;
        EBookState result = instance.getState();
         assertEquals(expResult, result);
    }

    /**
     * Test of repair method, of class Book.
     */
    @Test
    public void testRepair() {
        System.out.println("repair");
        Book instance = new Book(author, title, callNumber, bookID);
        
        ILoan loan =null;
        instance.borrow(loan);
        instance.returnBook(true);
        instance.repair();
        
        EBookState expResult = EBookState.AVAILABLE;
        EBookState result = instance.getState();
        assertEquals(expResult, result);
    }

    /**
     * Test of dispose method, of class Book.
     */
    @Test
    public void testDispose() {
        System.out.println("dispose");
        Book instance = new Book(author, title, callNumber, bookID);
        instance.dispose();
        EBookState expResult = EBookState.DISPOSED;
        EBookState result = instance.getState();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getState method, of class Book.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Book instance = new Book(author, title, callNumber, bookID);
        EBookState expResult = EBookState.AVAILABLE;
        EBookState result = instance.getState();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAuthor method, of class Book.
     */
    @Test
    public void testGetAuthor() {
        System.out.println("getAuthor");
        Book instance = new Book(author, title, callNumber, bookID);
        String expResult = author;
        String result = instance.getAuthor();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTitle method, of class Book.
     */
    @Test
    public void testGetTitle() {
        System.out.println("getTitle");
        Book instance = new Book(author, title, callNumber, bookID);
        String expResult = title;
        String result = instance.getTitle();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getCallNumber method, of class Book.
     */
    @Test
    public void testGetCallNumber() {
        System.out.println("getCallNumber");
        Book instance = new Book(author, title, callNumber, bookID);
        String expResult = callNumber;
        String result = instance.getCallNumber();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of getID method, of class Book.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        Book instance = new Book(author, title, callNumber, bookID);
        int expResult = bookID;
        int result = instance.getID();
        assertEquals(expResult, result);
        
    }
    
}
