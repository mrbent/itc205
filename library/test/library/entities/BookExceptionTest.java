
package library.entities;

import library.interfaces.entities.EBookState;
import library.interfaces.entities.ILoan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class BookExceptionTest {
   
    String author ="Writer Jackson";
    String title= "of drawers and piece ";
    String callNumber ="B42";
    int bookID=1;
    
    public BookExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    @Test (expected = RuntimeException.class)
    public void testBorrow() {
        System.out.println("borrow");
        ILoan loan = null;
        Book instance = new Book(author, title, callNumber, bookID);
        instance.borrow(loan);
        instance.borrow(loan);
        
        
    }

   
    @Test (expected = RuntimeException.class)
    public void testReturnBook() {
        System.out.println("returnBook");
        boolean damaged = true;
        Book instance = new Book(author, title, callNumber, bookID);
        
       instance.returnBook(damaged);
       EBookState expResult = EBookState.DAMAGED;
        EBookState result = instance.getState();
        
        assertEquals(expResult, result);
       
    }

    /**
     * Test of ReturnBook method, of class Book.
     */
   
    

    /**
     * Test of lose method, of class Book.
     */
    @Test(expected = RuntimeException.class)
    public void testLose() {
        System.out.println("lose");
        Book instance = null;
        instance.lose();
        
       
    }

    /**
     * Test of repair method, of class Book.
     */
    @Test(expected = RuntimeException.class)
    public void testRepair() {
        System.out.println("repair");
        Book instance = new Book(author, title, callNumber, bookID);
        
        ILoan loan =null;
        instance.borrow(loan);
        instance.returnBook(false);
        instance.repair();
        
       
        
    }

    /**
     * Test of dispose method, of class Book.
     */
    @Test(expected = RuntimeException.class)
    public void testDispose() {
        System.out.println("dispose");
        Book instance = new Book(author, title, callNumber, bookID);
        ILoan loan =null;
        instance.borrow(loan);
        instance.dispose();
         
        
    }

    
   
    
    
}
