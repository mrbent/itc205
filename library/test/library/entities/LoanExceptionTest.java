/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import library.interfaces.entities.ELoanState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class LoanExceptionTest {
    
    public LoanExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of commit method, of class Loan.
     */
    @Test(expected = RuntimeException.class)
    public void testCommitNotPending() {
        System.out.println("commit not pending");
       
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        
        Loan instance = new Loan(book,member, borrowDate, dueDate);
         
        
        int loanId = 1;
        instance.commit(loanId);
        instance.commit(loanId);
        
    }
    
    @Test(expected = RuntimeException.class)
    public void testCommitLessThanZero(){
        System.out.println("commit Less Than Zero");
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Calendar calendarCurrent = new GregorianCalendar(2016,7,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        Date currentDate = calendarCurrent.getTime();
        Loan instance = new Loan(book,member, borrowDate, dueDate);
       
        int loanId = 0;
        instance.commit(loanId);
        
    }

    /**
     * Test of complete method, of class Loan.
     */
    @Test(expected = RuntimeException.class)
    public void testComplete() {
        System.out.println("complete");
        Loan instance = null;
        instance.complete();
        
    }

    /**
     * Test of isOverDue method, of class Loan.
     */
    
    @Test(expected = RuntimeException.class)
    public void testCheckOverDue() {
        System.out.println("checkOverDue");
        Date currentDate = null;
        Loan instance = null;
        boolean expResult = false;
        boolean result = instance.checkOverDue(currentDate);
        assertEquals(expResult, result);
       
    }

   
    
    
}
