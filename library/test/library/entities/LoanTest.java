/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import library.interfaces.entities.ELoanState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class LoanTest {
    
    Loan instance;
   
    String author ="Writer Jackson";
    String title= "of drawers and piece ";
    String callNumber ="B42";
    int bookID=1;
    
    String firstName = "Billy";
    String lastName = "Bookington";
    String phoneNumber="555-999-839";
    String emailAddress = "dot@com.com";
    int memberID =1;
    
    Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
    Calendar calendarDue = new GregorianCalendar(2016,6,20);
    Calendar calendarCurrent = new GregorianCalendar(2016,7,20);
    Date borrowDate =calendarBorrow.getTime();
    Date dueDate =calendarDue.getTime();
    Date currentDate = calendarCurrent.getTime();
    
    
    
    public LoanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of commit method, of class Loan.
     */
    @Test
    public void testCommit() {
        System.out.println("commit");
        int loanId = 1;
        Book book = new Book(author, title, callNumber, bookID);
        Member member =  new Member(firstName, lastName,phoneNumber,emailAddress, memberID);
        
        
        
        instance = new Loan(book,member, borrowDate, dueDate);
        
        instance.commit(loanId);
       
    }

    /**
     * Test of complete method, of class Loan.
     */
    @Test
    public void testComplete() {
        System.out.println("complete");
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        instance = new Loan(book,member, borrowDate, dueDate);
        instance.commit(2);
        ELoanState expResult = ELoanState.CURRENT;
        ELoanState result = instance.getState();
        instance.complete();
        assertEquals(expResult,result);
    }

    /**
     * Test of isOverDue method, of class Loan.
     */
    @Test
    public void testIsOverDue() {
        System.out.println("isOverDue");
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
        instance = new Loan(book,member, borrowDate, dueDate);
        instance.commit(2);
        boolean expResult = false;
        boolean result = instance.isOverDue();
       
        assertEquals(expResult, result);
        
    }

    /**
     * Test of checkOverDue method, of class Loan.
     */
    @Test
    public void testCheckOverDue() {
        System.out.println("checkOverDue");
        
       
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        
      
       
       
        instance = new Loan(book,member, borrowDate, dueDate);
      
        instance.commit(2);
        boolean expResult = true;
        boolean result = instance.checkOverDue(currentDate);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of toString method, of class Loan.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Book book = new Book("Writer Jackson", "of drawers and piece ", "3orSomething", 9);
        Member member =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
      
        instance = new Loan(book,member, borrowDate, dueDate);
        
        String expResult = "Loan ID:  0\n" +
"Author:   Writer Jackson\n" +
"Title:    of drawers and piece \n" +
"Borrower: Billy Bookington\n" +
"Borrowed: 20/02/2016\n" +
"Due Date: 20/07/2016";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}
