/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.List;
import library.entities.Book;
import library.interfaces.entities.IBook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class BookMapDAOTest {
    
     BookHelper helper = new BookHelper();
     BookMapDAO instance = new BookMapDAO(helper);
    
     String author="Writer Jackson";
     String title = "of drawers and piece";
     String callNumber = "B42";
    
    public BookMapDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addBook method, of class BookMapDAO.
     */
    @Test
    public void testAddBook() {
        System.out.println("addBook");
      
       
        
        
        IBook result = instance.addBook(author,title, callNumber);
        assertTrue(result instanceof Book);
       
    }

    /**
     * Test of getBookByID method, of class BookMapDAO.
     */
    @Test
    public void testGetBookByID() {
        System.out.println("getBookByID");
        
        IBook expResult = instance.addBook(author,title, callNumber);
        int id = 1;
        
        IBook result = instance.getBookByID(id);
       
        assertEquals(expResult ,result );
    }

  
   

    /**
     * Test of findBooksByAuthor method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByAuthor() {
        System.out.println("findBooksByAuthor");
      
        instance.addBook(author,title, callNumber);
       
        List<IBook> result = instance.findBooksByAuthor(author);
        
        assertTrue( result.get(0) instanceof Book);
       
    }

    /**
     * Test of findBooksByTitle method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByTitle() {
        System.out.println("findBooksByTitle");
        
        instance.addBook(author,title, callNumber);
        
        List<IBook> result = instance.findBooksByTitle(title);
        
        assertTrue( result.get(0) instanceof Book);
    }

    /**
     * Test of findBooksByAuthorTitle method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByAuthorTitle() {
        System.out.println("findBooksByAuthorTitle");
        
        instance.addBook(author,title, callNumber);
        
        List<IBook> result = instance.findBooksByAuthorTitle(author, title);
      
        assertTrue( result.get(0) instanceof Book);
    }
    
}
