/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import library.entities.Book;
import library.entities.Loan;
import library.entities.Member;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class LoanHelperTest {
    
     String author="Writer Jackson";
     String title = "of drawers and piece";
     String callNumber = "B42";
     int LoanID=1;
    
    public LoanHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of makeLoan method, of class LoanHelper.
     */
    @Test
    public void testMakeLoan() {
        System.out.println("makeLoan");
        Book book = new Book(author, title, callNumber, LoanID);
       
        Member borrower =  new Member("Billy", "Bookington","555-999-839","thisIsMyEmail@email.net", 42);
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Date borrowDate = calendarBorrow.getTime();
        Date dueDate  =calendarDue.getTime();
        
        LoanHelper instance = new LoanHelper();
        ILoan result = instance.makeLoan(book, borrower, borrowDate, dueDate);
        assertTrue(result instanceof Loan);
        
       
    }
    
}
