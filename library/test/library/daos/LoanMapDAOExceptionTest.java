/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.List;
import library.entities.Member;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class LoanMapDAOExceptionTest {
    
    String firstName = "Billy";
    String lastName = "Bookington";
    String phoneNumber="555-999-839";
    String emailAddress = "dot@com.com";
    int memberID = 1;
    
    
    public LoanMapDAOExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  
    /**
     * Test of getLoanByBook method, of class LoanMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetLoanByBook() {
        System.out.println("getLoanByBook");
        LoanHelper helper = new LoanHelper();
        LoanMapDAO instance = new LoanMapDAO(helper);
        
        
        IBook book = null;
        Member member = new Member (firstName, lastName,phoneNumber, emailAddress,memberID);
        ILoan expResult = instance.createLoan(member,book);
       
        ILoan result = instance.getLoanByBook(book);
        assertEquals(expResult, result);
       
    }

    
    /**
     * Test of findLoansByBorrower method, of class LoanMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindLoansByBorrower() {
        System.out.println("findLoansByBorrower");
        LoanHelper helper = new LoanHelper();
        LoanMapDAO instance = new LoanMapDAO(helper);
        
        
        IMember borrower = null;
       
        List<ILoan> expResult = null;
        List<ILoan> result = instance.findLoansByBorrower(borrower);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of findLoansByBookTitle method, of class LoanMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindLoansByBookTitle() {
        System.out.println("findLoansByBookTitle");
        LoanHelper helper = new LoanHelper();
        LoanMapDAO instance = new LoanMapDAO(helper);
        
        
        String title = null;
      
        List<ILoan> expResult = null;
        List<ILoan> result = instance.findLoansByBookTitle(title);
        assertEquals(expResult, result);
        
    }


    /**
     * Test of createLoan method, of class LoanMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testCreateLoan() {
        System.out.println("createLoan");
        LoanHelper helper = new LoanHelper();
        LoanMapDAO instance = new LoanMapDAO(helper);
        
        
        IMember borrower = null;
        IBook book = null;
       
        ILoan expResult = null;
        ILoan result = instance.createLoan(borrower, book);
        assertEquals(expResult, result);
        
    }

}
