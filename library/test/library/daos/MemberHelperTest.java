/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import library.entities.Member;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class MemberHelperTest {
    
    public MemberHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of makeMember method, of class MemberHelper.
     */
    @Test
    public void testMakeMember() {
        System.out.println("makeMember");
        String firstName = "Billy";
        String lastName = "Bookington";
        String phoneNumber="555-999-839";
        String emailAddress = "dot@com.com";
        int id = 1;
        
        
        MemberHelper instance = new MemberHelper();
        IMember result = instance.makeMember(firstName, lastName, phoneNumber, emailAddress, id);
        assertTrue(result instanceof Member);
       
    }
    
}
