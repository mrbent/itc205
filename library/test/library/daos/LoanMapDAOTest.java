/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import library.entities.Book;
import library.entities.Loan;
import library.entities.Member;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class LoanMapDAOTest {
    
       LoanHelper helper = new LoanHelper();
       LoanMapDAO instance = new LoanMapDAO(helper);
    
    
        String title = "of drawers and piece ";
        String author ="Writer Jackson";
        String callNumber ="3QA";
        int bookID =9;
        
        Book book = new Book(author, title, callNumber, bookID);
       
        String firstName="Billy";
        String lastName="Bookington";
        String phoneNumber="555-999-839";
        String email = "thisIsMyEmail@email.net";
        int memberID=1;
        
         Member borrower  = new Member (firstName, lastName, phoneNumber, email,memberID);
    
    public LoanMapDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLoanByID method, of class LoanMapDAO.
     */
    @Test
    public void testGetLoanByID() {
        System.out.println("getLoanByID");
        
        ILoan expResult = instance.createLoan(borrower, book);
        instance.commitLoan(expResult);
        ILoan result = instance.getLoanByID(memberID);
        
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getLoanByBook method, of class LoanMapDAO.
     */
    @Test
    public void testGetLoanByBook() {
        System.out.println("getLoanByBook");
      
        ILoan expResult = instance.createLoan(borrower,book);
        instance.commitLoan(expResult);
        ILoan result = instance.getLoanByBook(book);
        
        assertEquals(expResult, result); 
    }

    /**
     * Test of listLoans method, of class LoanMapDAO.
     */
    @Test
    public void testListLoans() {
        System.out.println("listLoans");
       
        List<ILoan> expResult = new ArrayList<ILoan>();
        
        List<ILoan> result = instance.listLoans();
       
        assertEquals(expResult, result);
       
    }

    /**
     * Test of findLoansByBorrower method, of class LoanMapDAO.
     */
    @Test
    public void testFindLoansByBorrower() {
        System.out.println("findLoansByBorrower");
    
        ILoan expResult = instance.createLoan(borrower,book);
        instance.commitLoan(expResult);
       
        List<ILoan> result = instance.findLoansByBorrower(borrower);
        
        assertEquals(expResult,result.get(0));
        
    }

    /**
     * Test of findLoansByBookTitle method, of class LoanMapDAO.
     */
    @Test
    public void testFindLoansByBookTitle() {
        System.out.println("findLoansByBookTitle");
    
        ILoan expResult = instance.createLoan(borrower,book);
        instance.commitLoan(expResult);
    
        List<ILoan> result = instance.findLoansByBookTitle(title);
        assertEquals(expResult, result.get(0));
        
    }

    /**
     * Test of updateOverDueStatus method, of class LoanMapDAO.
     */
    @Test
    public void testUpdateOverDueStatus() {
        System.out.println("updateOverDueStatus");
        
        
         Calendar calendar = new GregorianCalendar(2016,16,20);
          
         Date currentDate =calendar.getTime();
        
        instance.updateOverDueStatus(currentDate);
       
    }

    /**
     * Test of findOverDueLoans method, of class LoanMapDAO.
     */
    @Test
    public void testFindOverDueLoans() {
        System.out.println("findOverDueLoans");
    
        
        Calendar calendar = new GregorianCalendar(2016,19,20);
          
        Date currentDate =calendar.getTime();
        
        ILoan expResult = instance.createLoan(borrower,book);
        instance.commitLoan(expResult);
        instance.updateOverDueStatus(currentDate);
        List<ILoan> result = instance.findOverDueLoans();
        assertEquals(expResult, result.get(0));
        
        
    
    }

    /**
     * Test of createLoan method, of class LoanMapDAO.
     */
    @Test
    public void testCreateLoan() {
        System.out.println("createLoan");
     
        ILoan result = instance.createLoan(borrower, book);
       
        assertTrue(result instanceof Loan);
        
    }

    /**
     * Test of commitLoan method, of class LoanMapDAO.
     */
    @Test
    public void testCommitLoan() {
        System.out.println("commitLoan");
     
        ILoan result = instance.createLoan(borrower, book);
        // EBookState expResult=EBookState.ON_LOAN;

        instance.commitLoan(result);
        assertTrue(result instanceof Loan);
        
    }
    
}