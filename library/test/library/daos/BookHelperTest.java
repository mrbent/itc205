/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import library.entities.Book;
import library.interfaces.entities.IBook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class BookHelperTest {
    
     String author="Writer Jackson";
     String title = "of drawers and piece";
     String callNumber = "B42";
     int bookID=1;
     
    public BookHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of makeBook method, of class BookHelper.
     */
    @Test
    public void testMakeBook() {
        System.out.println("makeBook");
      
        BookHelper instance = new BookHelper();
       
        IBook result = instance.makeBook(author, title, callNumber, 1);
        assertTrue(result instanceof Book);
        
    }
    
}
