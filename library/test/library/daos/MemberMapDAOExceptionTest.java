/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.List;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class MemberMapDAOExceptionTest {
    
     String firstName = null;
     String lastName = null;
     String emailAddress = null;
    
    MemberHelper helper = new MemberHelper();
    MemberMapDAO instance = new MemberMapDAO(helper);
    
    public MemberMapDAOExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    /**
     * Test of findMembersByLastName method, of class MemberMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindMembersByLastName() {
        System.out.println("findMembersByLastName");
        
        
        
        List<IMember> expResult = null;
        List<IMember> result = instance.findMembersByLastName(lastName);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of findMembersByEmailAddress method, of class MemberMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindMembersByEmailAddress() {
        System.out.println("findMembersByEmailAddress");
        
        
       
        List<IMember> expResult = null;
        List<IMember> result = instance.findMembersByEmailAddress(emailAddress);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of findMembersByNames method, of class MemberMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindMembersByNames() {
        System.out.println("findMembersByNames");
        
       
      
       IMember expResult = instance.addMember(firstName, lastName, "000", "dot@com.com");
       List<IMember> result = instance.findMembersByNames(firstName, lastName);
        
        assertEquals(expResult, result.get(0));
       
    }
    
}
