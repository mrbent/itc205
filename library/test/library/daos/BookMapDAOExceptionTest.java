/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.List;
import library.interfaces.entities.IBook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class BookMapDAOExceptionTest {
    
    BookHelper helper = new BookHelper();
    BookMapDAO instance = new BookMapDAO(helper);
    
    String author = "";
    String title = "";
    
    public BookMapDAOExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

   

   
    /**
     * Test of findBooksByAuthor method, of class BookMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindBooksByAuthor() {
        System.out.println("findBooksByAuthor");
        
        List<IBook> expResult = null;
        List<IBook> result = instance.findBooksByAuthor(author);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of findBooksByTitle method, of class BookMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindBooksByTitle() {
        System.out.println("findBooksByTitle");
        
        List<IBook> expResult = null;
        List<IBook> result = instance.findBooksByTitle(title);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of findBooksByAuthorTitle method, of class BookMapDAO.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFindBooksByAuthorTitle() {
        System.out.println("findBooksByAuthorTitle");
      
        List<IBook> expResult = null;
        List<IBook> result = instance.findBooksByAuthorTitle(author, title);
        assertEquals(expResult, result);
        
    }
    
}
