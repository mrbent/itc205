/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos;

import java.util.List;
import library.entities.Member;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bendev
 */
public class MemberMapDAOTest {
    
    MemberHelper helper = new MemberHelper();
    MemberMapDAO instance = new MemberMapDAO(helper);
   
    String firstName = "Billy";
    String lastName = "Bookington";
    String contactPhone = "000";
    String emailAddress = "dot@com.com";
    
    public MemberMapDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMember method, of class MemberMapDAO.
     */
    @Test
    public void testAddMember() {
        System.out.println("addMember");
        System.out.println("makeMember");
        
        
        
        IMember result = instance.addMember(firstName, lastName, contactPhone, emailAddress);
        assertTrue(result instanceof Member);
       
    }

    /**
     * Test of getMemberByID method, of class MemberMapDAO.
     */
    @Test
    public void testGetMemberByID() {
        System.out.println("getMemberByID");
        int id = 1;
        System.out.println("addMember");
        System.out.println("makeMember");

        IMember expResult = instance.addMember(firstName, lastName, contactPhone, emailAddress);
        IMember result = instance.getMemberByID(id);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of listMembers method, of class MemberMapDAO.
     */
    @Test
    public void testListMembers() {
        System.out.println("listMembers");
        
        List<IMember> result = instance.listMembers();
        assertTrue(result instanceof List);
       
    }

    /**
     * Test of findMembersByLastName method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByLastName() {
        System.out.println("findMembersByLastName");
       
        IMember expResult = instance.addMember(firstName, lastName, contactPhone, emailAddress);
        List<IMember> result = instance.findMembersByLastName(lastName);
        assertEquals(expResult, result.get(0));
        
    }

    /**
     * Test of findMembersByEmailAddress method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByEmailAddress() {
        System.out.println("findMembersByEmailAddress");
       
        IMember expResult = instance.addMember(firstName, lastName, contactPhone, emailAddress);
        List<IMember> result = instance.findMembersByEmailAddress(emailAddress);
        
        assertEquals(expResult, result.get(0));
        
    }

    /**
     * Test of findMembersByNames method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByNames() {
        System.out.println("findMembersByNames");
        
        IMember expResult = instance.addMember(firstName, lastName, contactPhone, emailAddress);
        List<IMember> result = instance.findMembersByNames(firstName, lastName);
        
        assertEquals(expResult, result.get(0));
        
    }
    
}
