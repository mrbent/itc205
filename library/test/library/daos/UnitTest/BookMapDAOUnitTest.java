/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos.UnitTest;

import java.util.List;
import library.daos.BookHelper;
import library.daos.BookMapDAO;
import library.entities.Book;
import library.interfaces.entities.IBook;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author bendev
 */
public class BookMapDAOUnitTest {
        
    BookHelper helper = mock(BookHelper.class);
    Book book= mock(Book.class);
    BookMapDAO instance = new BookMapDAO(helper);
    
    String author ="Writer Jackson";
    String title= "of drawers and piece ";
    String callNumber ="B42";
    int bookID=1;
    
    
  
    
    public BookMapDAOUnitTest() {
    }
   
    
    @Before
    public void setUp() {
          
        
        when(helper.makeBook(author, title, callNumber, bookID)).thenReturn(book);
        
        when(book.getAuthor()).thenReturn(author);
        when(book.getTitle()).thenReturn(title);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addBook method, of class BookMapDAO.
     */
    @Test
    public void testAddBook() {
        System.out.println("addBook");
      
        
        assertEquals(book, instance.addBook(author, title, callNumber));
       
    }

    /**
     * Test of getBookByID method, of class BookMapDAO.
     */
    @Test
    public void testGetBookByID() {
        System.out.println("getBookByID");
        
        when(helper.makeBook(author, title, callNumber, bookID)).thenReturn(book);
        instance.addBook(author, title, callNumber);
        
        
       
        IBook result = instance.getBookByID(bookID);
       
        assertEquals(book,result);
    }

  
   

    /**
     * Test of findBooksByAuthor method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByAuthor() {
        System.out.println("findBooksByAuthor");
        
      
       instance.addBook(author, title, callNumber);
        
       instance.findBooksByAuthor(author);
       
       List<IBook> result = instance.findBooksByAuthor(author);
        
        assertEquals( book,result.get(0));
       
    }

    /**
     * Test of findBooksByTitle method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByTitle() {
        System.out.println("findBooksByTitle");
     
      
       instance.addBook(author, title, callNumber);
       
       

        List<IBook> result = instance.findBooksByTitle(title);
        
        assertTrue( result.get(0) instanceof Book);
    }

    /**
     * Test of findBooksByAuthorTitle method, of class BookMapDAO.
     */
    @Test
    public void testFindBooksByAuthorTitle() {
        System.out.println("findBooksByAuthorTitle");
       
        instance.addBook(author, title, callNumber);
        
        List<IBook> result = instance.findBooksByAuthorTitle(author, title);
       
        assertEquals( book, result.get(0));
    }
    
}
