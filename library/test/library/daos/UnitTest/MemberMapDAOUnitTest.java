/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos.UnitTest;

import java.util.List;
import library.daos.MemberHelper;
import library.daos.MemberMapDAO;
import library.entities.Member;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

/**
 *
 * @author bendev
 */
public class MemberMapDAOUnitTest {
    
    MemberHelper helper = mock(MemberHelper.class);
    Member member= mock(Member.class);
    MemberMapDAO instance = new MemberMapDAO(helper);
    
    String firstName = "Billy";
    String lastName = "Bookington";
    String phoneNumber = "000";
    String emailAddress = "dot@com.com";
    int id = 1;
    
    
    
    public MemberMapDAOUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        when(helper.makeMember(firstName, lastName, phoneNumber, emailAddress, id)).thenReturn(member);
      
        when(member.getEmailAddress()).thenReturn(emailAddress);
        when(member.getFirstName()).thenReturn(firstName);
        when(member.getLastName()).thenReturn(lastName);
        when(member.getContactPhone()).thenReturn(phoneNumber);
        when(member.getEmailAddress()).thenReturn(emailAddress);
        when(member.getID()).thenReturn(id);
       
                
      
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMember method, of class MemberMapDAO.
     */
    @Test
    public void testAddMember() {
        System.out.println("addMember");
   
        
        
      
       instance = new MemberMapDAO(helper);
        
        IMember result = instance.addMember(firstName, lastName, phoneNumber, emailAddress);
        assertEquals(member,result);
       
    }

    /**
     * Test of getMemberByID method, of class MemberMapDAO.
     */
    @Test
    public void testGetMemberByID() {
        System.out.println("getMemberByID");
        
        instance.addMember(firstName, lastName, phoneNumber, emailAddress); 
        IMember result = instance.getMemberByID(id);
        assertEquals(member, result);
        
    }

    /**
     * Test of listMembers method, of class MemberMapDAO.
     */
    @Test
    public void testListMembers() {
        System.out.println("listMembers");
        
         instance.addMember(firstName, lastName, phoneNumber, emailAddress); 
        List<IMember> result = instance.listMembers();
        assertEquals(member,result.get(0));
       
    }

    /**
     * Test of findMembersByLastName method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByLastName() {
        System.out.println("findMembersByLastName");
        
        when(helper.makeMember(firstName, lastName, phoneNumber, emailAddress, id)).thenReturn(member);
       
        IMember expResult = instance.addMember(firstName, lastName, phoneNumber, emailAddress);
        List<IMember> result = instance.findMembersByLastName(lastName);
        assertEquals(member, result.get(0));
        
    }

    /**
     * Test of findMembersByEmailAddress method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByEmailAddress() {
        System.out.println("findMembersByEmailAddress");
       
        when(helper.makeMember(firstName, lastName, phoneNumber, emailAddress, id)).thenReturn(member);
       
        IMember expResult = instance.addMember(firstName, lastName, phoneNumber, emailAddress);
        List<IMember> result = instance.findMembersByEmailAddress(emailAddress);
        
        assertEquals(member, result.get(0));
        
    }

    /**
     * Test of findMembersByNames method, of class MemberMapDAO.
     */
    @Test
    public void testFindMembersByNames() {
        System.out.println("findMembersByNames");
        
        when(helper.makeMember(firstName, lastName, phoneNumber, emailAddress, id)).thenReturn(member);
       
        IMember expResult = instance.addMember(firstName, lastName, phoneNumber, emailAddress);
        List<IMember> result = instance.findMembersByNames(firstName, lastName);
        assertEquals(member, result.get(0));
        
    }
    
}
