/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.daos.UnitTest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import library.daos.LoanHelper;
import library.daos.LoanMapDAO;
import library.entities.Book;
import library.entities.Loan;
import library.entities.Member;
import library.interfaces.entities.ELoanState;
import library.interfaces.entities.IBook;
import library.interfaces.entities.ILoan;
import library.interfaces.entities.IMember;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

/**
 *
 * @author bendev
 */
public class LoanMapDAOUnitTest {
       
       LoanHelper helper = mock(LoanHelper.class);
       Loan loan= mock(Loan.class);
       LoanMapDAO instance = new LoanMapDAO(helper);
    
    
        String title = "of drawers and piece ";
        String author ="Writer Jackson";
        String callNumber ="3QA";
        int bookID =1;
        
       IBook book = mock(IBook.class);
       
        String firstName="Billy";
        String lastName="Bookington";
        String phoneNumber="555-999-839";
        String email = "thisIsMyEmail@email.net";
        int memberID=1;
        
        IMember borrower  = mock(IMember.class);
        
        Calendar calendarBorrow = new GregorianCalendar(2016,1,20);
        Calendar calendarDue = new GregorianCalendar(2016,6,20);
        Calendar calendarCurrent = new GregorianCalendar(2016,7,20);
        Date borrowDate =calendarBorrow.getTime();
        Date dueDate =calendarDue.getTime();
        Date currentDate = calendarCurrent.getTime();
        int loanID=1;
        
        
        
    
    public LoanMapDAOUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        when(helper.makeLoan(book, borrower, borrowDate, dueDate)).thenReturn(loan);
       
        when(loan.getBook()).thenReturn(book);
        when(book.getTitle()).thenReturn(title);
       
        
       instance.createLoan(borrower,book);
       instance.commitLoan(loan);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLoanByID method, of class LoanMapDAO.
     */
    @Test
    public void testGetLoanByID() {
        System.out.println("getLoanByID");
        //when(helper.makeBook(author, title, callNumber, bookID)).thenReturn(book);
       
         when(loan.getID()).thenReturn(bookID).thenReturn(loanID);
       
        
        ILoan result = instance.getLoanByID(loanID);
        
        assertEquals(loan, result);
        
    }

    /**
     * Test of getLoanByBook method, of class LoanMapDAO.
     */
    @Test
    public void testGetLoanByBook() {
        System.out.println("getLoanByBook");
      
       when(loan.getBook()).thenReturn(book);
       
        ILoan result = instance.getLoanByBook(book);
        
        assertEquals(loan, result); 
    }

    /**
     * Test of listLoans method, of class LoanMapDAO.
     */
    @Test
    public void testListLoans() {
        System.out.println("listLoans");
       
        
        
        List<ILoan> expResult = new ArrayList<ILoan>();
        
        List<ILoan> result = instance.listLoans();
       
        assertEquals(loan, result.get(0));
       
    }

    /**
     * Test of findLoansByBorrower method, of class LoanMapDAO.
     */
    @Test
    public void testFindLoansByBorrower() {
        System.out.println("findLoansByBorrower");
    
       when(loan.getBorrower()).thenReturn(borrower);
        
        List<ILoan> result = instance.findLoansByBorrower(borrower);
        
        assertEquals(loan,result.get(0));
        
    }

    /**
     * Test of findLoansByBookTitle method, of class LoanMapDAO.
     */
    @Test
    public void testFindLoansByBookTitle() {
        System.out.println("findLoansByBookTitle");
      
        
         
        List<ILoan> result = instance.findLoansByBookTitle(title);
        assertEquals(loan, result.get(0));
        
    }

    /**
     * Test of updateOverDueStatus method, of class LoanMapDAO.
     */
    @Test
    public void testUpdateOverDueStatus() {
        System.out.println("updateOverDueStatus");
        
      
        instance.updateOverDueStatus(currentDate);
       
    }

    /**
     * Test of findOverDueLoans method, of class LoanMapDAO.
     */
    @Test
    public void testFindOverDueLoans() {
        System.out.println("findOverDueLoans");
    
        
        when(loan.isOverDue()).thenReturn(true);
       
       
        instance.updateOverDueStatus(currentDate);
        List<ILoan> result = instance.findOverDueLoans();
        assertEquals(loan, result.get(0));
        
        
    
    }

  
   
    
}